# Debugging
# ------------------------------------------------------------------------------

if server? then require 'pry' end

# Locale Configuration
# ------------------------------------------------------------------------------

# Supported Locales
# Set Korean as the default locale in production,
# but set English as default in dev environment
config[:supported_locales] = build? ? [:ko, :en] : [:en, :ko]

# Directory Structure
# ------------------------------------------------------------------------------

# Group CSS/IMG/JS into Assets folder
config[:css_dir]    = 'assets/stylesheets'
config[:images_dir] = 'assets/images'
config[:js_dir]     = 'assets/javascripts/dist'

# Prevent Middleman from adding our dev JS to the Middleman sitemap;
# we only our compiled JS to be avaiable to the Middleman Sitemap.
ignore 'assets/javascripts/src/*'

# Make sure we can access Node packages from within SCSS
# config[:sass_assets_paths] = [File.join(root, 'node_modules')]

# Layouts
# https://middlemanapp.com/basics/layouts/
# ------------------------------------------------------------------------------

# Per-page layout changes
# page '/*.xml', layout: false
# page '/*.json', layout: false
# page '/*.txt', layout: false

# With alternative layout
# page '/path/to/file.html', layout: 'other_layout'

# Proxy pages
# https://middlemanapp.com/advanced/dynamic-pages/
# ------------------------------------------------------------------------------

# proxy(
#   '/this-page-has-no-template.html',
#   '/template-file.html',
#   locals: {
#     which_fake_page: 'Rendering a fake page with a local variable'
#   },
# )

# Helpers
# Methods defined in the helpers block are available in templates
# https://middlemanapp.com/basics/helper-methods/
# ------------------------------------------------------------------------------

# helpers do
#   def some_helper
#     'Helping'
#   end
# end

# Contentful mappers
# ------------------------------------------------------------------------------

# Maps Contentful Cafe entries to GeoJSON-structured data
class CafeGeoJSONMapper
  def initialize(entries, options)
    @entries = entries
    @options = options
  end

  def map(context, entry)
    context.type = 'Feature'

    # Number-based IDs are required in GeoJSON,
    # so use them here instead of the Contentful ID.
    context.id = @entries.find_index(entry)

    context.geometry = {
      type: 'Point',
      coordinates: [
        entry.coordinates.lon,
        entry.coordinates.lat,
      ]
    }

    # 1 - This is the Contentful ID, not the context ID used above
    context.properties = {
      address_ko: entry.fields_with_locales[:address][:ko],
      address_en: entry.fields_with_locales[:address][:'en-US'],
      id: entry.id, # 1
      image: entry.image.fields[:file].url,
      instagram: entry.try(:instragram),
      name_ko: entry.fields_with_locales[:name][:ko],
      name_en: entry.fields_with_locales[:name][:'en-US'],
    }
  end
end

# Maps Contentful Cleanup entries to simpler objects
class CleanupMapper
  def initialize(entries, options)
    @entries = entries
    @options = options
  end

  def map(context, entry)
    context.date = entry.date
    context.coordinates = [
      entry.location.lon,
      entry.location.lat,
    ]
    context.placename_ko = entry.fields_with_locales[:place_name][:ko]
    context.placename_en = entry.fields_with_locales[:place_name][:'en-US']
    context.volunteers = entry.volunteers
    context.waste_collected = entry.waste_collected
  end
end

# Activate and configure extensions
# https://middlemanapp.com/advanced/configuration/#configuring-extensions
# ------------------------------------------------------------------------------

activate :autoprefixer do |prefix|
  prefix.browsers = 'last 2 versions'
end

activate :protect_emails

activate :i18n,
  langs: config[:supported_locales],
  templates_dir: 'templates'

activate :contentful do |instance|
  instance.space = { contentful: ENV['CONTENTFUL_SPACE_ID'] }
  instance.access_token = ENV['CONTENTFUL_DELIVERY_ACCESS_TOKEN']
  instance.cda_query = { locale: '*' }
  instance.content_types = {
    cafes: {
      id: 'partner',
      mapper: CafeGeoJSONMapper,
    },
    cleanups: {
      id: 'event',
      mapper: CleanupMapper,
    },
  }
end

# Compile ES6-based JavaScript to ES5 via Webpack/Babel
webpack_location = './node_modules/webpack/bin/webpack.js'
webpack_command = "#{ webpack_location } --bail --mode=production"
webpack_command = "#{ webpack_location } --watch --mode=development" if server?

activate :external_pipeline,
  name: :webpack,
  command: webpack_command,
  source: '.tmp/webpack'

# Show `/foo.html` as `/foo/`
# activate :directory_indexes

# Build-specific configuration
# https://middlemanapp.com/advanced/configuration/#environment-specific-settings
# ------------------------------------------------------------------------------

configure :development do
  # LiveReload
  # Refresh the browser automatically on file save
  # NOTE: Ignore dev JS, since it always triggers Webpack to compile;
  # the compiled JS will then trigger LiveReload.
  activate :livereload, ignore: [
    /\/assets\/javascripts\/src/,
    /\/node_modules/,
  ]
end

configure :build do
  activate :asset_hash
  activate :minify_css
  activate :minify_html
  activate :minify_javascript
end
