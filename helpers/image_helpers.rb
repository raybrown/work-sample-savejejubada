# Middleman - Inline SVG Helper
# ------------------------------------------------------------------------------
#
# Installation
# ------------
# 1. Save this file at `[project_root]/helpers/image_helpers.rb`
# 2. Open the project's Gemfile and add this line: `gem "oga"`
# 3. Run `bundle install` from the command line
#
# Note: Restart your local Middleman server (if it's running) before continuing
#
# Usage
# -----
# Embed SVG files into your template files like so:
#
# ```
# <%= inline_svg "name/of/file.svg" %>
# ```
#
# The helper also allows you to pass attributes to add to the SVG tag, like so:
#
# Input:
# ```
# <%= inline_svg "name/of/file.svg", class: "foo", data: {bar: "baz"} %>
# ```
#
# Output:
# ```
# <svg <!-- existing attributes --> class="foo" data-bar="baz">
#   <!-- SVG contents -->
# </svg>
# ```
#
# Acknowledgements and Contributors
# --------------------------
# This was initally adapted from the work of James Martin
# and Steven Harley, which you can read more about here:
# https://robots.thoughtbot.com/organized-workflow-for-svg
#
# Further improvements were made based on contributions by:
#
# * Cecile Veneziani (@cveneziani)
# * Allan White (@allanwhite)
#
# Thanks for improving this helper! Have questions or concerns?
# Feel free to fork the Gist or comment on it here:
# https://gist.github.com/bitmanic/0047ef8d7eaec0bf31bb
module ImageHelpers
  def inline_svg(relative_image_path, optional_attributes = {})
    image_path = File.join(
      config[:source],
      config[:images_dir],
      relative_image_path
    )

    # Open the file
    image = File.open(image_path, 'r') { |f| f.read }

    # Return the file's contents if no optional attributes were passed in
    return image if optional_attributes.empty?

    # Otherwise, parse the file's SVG code
    document = Oga.parse_xml(image)
    svg = document.css('svg').first

    # Regardless of root element, add the optional attributes to it
    add_attributes_to_element svg, optional_attributes

    # Finally, return the SVG code
    document.to_xml
  end

  def inline_svg_sprite(name, classes = "icon", view_box = "0 0 100 100")
    <<~HTML
      <svg class="#{ classes }" viewBox="#{ view_box }">
        <use xlink:href="##{ name }"></use>
      </svg>
    HTML
  end

  def inline_svg_as_symbol(relative_image_path, id)
    svg_code = inline_svg relative_image_path, { id: id }

    # Parse the SVG code so that we can manipulate it
    document = Oga.parse_xml(svg_code)
    svg = document.css('svg').first

    # Remove all attributes besides ID
    svg.attributes.each do |attribute|
      svg.unset(attribute.name) unless attribute.name === "id"
    end

    # Flatten the SVG code to a string again
    svg_code = svg.to_xml

    # Replace `<svg>` tag with `<symbol>`
    svg_code
      .gsub('<svg', '<symbol')
      .gsub('</svg>', '</symbol>')
  end

  private

  def add_attributes_to_element(element, attributes)
    attributes.each do |attribute, value|
      case value

      # NOTE: This allows for hash-based values, but we're only going one level
      #       deep right now. If you know a great way to dig `N` levels deeper,
      #       feel free to post about it on the Gist.
      when Hash
        value.each do |subattribute, subvalue|
          unless subvalue.class == Hash
            element.set(
              "#{attribute} #{subattribute}".parameterize,
              subvalue.html_safe
            )
          end
        end

      else
        element.set(attribute.to_sym, value.html_safe)
      end
    end
  end
end
