module MapboxHelpers
  def static_map_url(dimensions: '1280x900', zoom: 10, pins: [])
    access_token = ENV['MAPBOX_ACCESS_TOKEN']
    lat = '33.49301'
    lon = '126.51329'
    style_user = 'bitmanic'
    style_id = 'cjk3cekd540lt2rpevv8p58j7'

    url = "https://api.mapbox.com/styles/v1/"
    url << "#{ style_user }/#{ style_id }/static/"

    unless pins.empty?
      pin_strings = []

      pins.each do |pin|
        lon = pin[0].round(3)
        lat = pin[1].round(3)

        pin_strings << "pin-l-cafe+444(#{ lon },#{ lat })"
      end

      url << "#{ pin_strings.join(',') }/"
    end

    url << "#{ lon },#{ lat },#{ zoom },0,0/"
    url << "#{ dimensions }@2x?access_token=#{ access_token }"

    url.strip
  end

  def feature_coordinates_array(features)
    arr = []

    features.each do |id, feature|
      arr.push([
        feature[:geometry][:coordinates][0],
        feature[:geometry][:coordinates][1]
      ])
    end

    arr
  end
end
