const path = require('path');
const VueLoaderPlugin = require('vue-loader/lib/plugin');

module.exports = {
  entry: './source/assets/javascripts/src/site.js',

  // We output the main script file to the `.tmp/webpack/` folder, but we mimic
  // Middleman's script folder naming convention within that `.tmp/webpack/`
  // folder. Why? Well, we use Middleman's external pipeline feature to compile
  // ES6-based JS to ES5, since Middleman itself doesn't handle this
  // (or ever intend to).
  //
  // With the external pipeline feature, Middleman will look in the
  // `.tmp/webpack/` folder and load whatever it finds there to the Middleman
  // Sitemap (which you can see at http://localhost:4567/__middleman/sitemap/
  // whenever Middleman is running). We want Middleman to find the compiled ES5
  // JS and then load it as it if were inside the `source/assets/javascripts`
  // folder. This keeps our JS in one place, and `middleman build` will generate
  // an actual JS file in that location, just as we'd expect it to.
  output: {
    filename: 'assets/javascripts/dist/site.js',
    path: path.resolve(__dirname, '.tmp/webpack')
  },

  module: {
    rules: [
      // Plain JS files
      {
        test: /\.js$/,
        exclude: /(node_modules|vendor)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env']
          }
        }
      },

      // Vue single file components (SFC)
      {
        test: /\.vue$/,
        loader: 'vue-loader'
      },

      // Plain CSS and style blocks within Vue SFC
      {
        test: /\.css$/,
        use: [
          'vue-style-loader',
          'css-loader'
        ]
      }
    ]
  },

  plugins: [
    new VueLoaderPlugin()
  ],

  resolve: {
    alias: {
      vue: 'vue/dist/vue.js'
    }
  },

  watchOptions: {
    ignored: [
      'node_modules'
    ]
  }
};
