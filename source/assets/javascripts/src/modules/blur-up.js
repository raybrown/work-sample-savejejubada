// Add "blur-up" image loading. For more info, see:
// https://github.com/aFarkas/lazysizes/tree/master/plugins/blur-up
import 'lazysizes/plugins/object-fit/ls.object-fit';
import 'lazysizes/plugins/parent-fit/ls.parent-fit';
import 'lazysizes/plugins/blur-up/ls.blur-up';
import 'lazysizes';
