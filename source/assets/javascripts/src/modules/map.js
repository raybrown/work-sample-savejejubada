import Vue from 'vue';
import Map from '../components/Map.vue';
import { matchesDeep } from '../_utils';

export default class {
  constructor(elem) {
    this.$map = elem;
    this.data = {
      interactive: elem.dataset.mapInteractive === 'true',
      marker: elem.dataset.mapMarker,
      source: elem.dataset.mapSource,
      sourceName: elem.dataset.mapSourceName,
    };
    this.$vm = null;
  }

  init() {
    this.$vm = new Vue({
      el: this.$map,
      components: { Map },
      data: this.data,
      render: function(createElement) { return createElement(Map); },
    });

    document.addEventListener('click', function(event) {
      if (matchesDeep(event.target, '[data-map-toggle]')) {
        this.$vm.$children[0].toggle();
      }
    }.bind(this));
  }
}
