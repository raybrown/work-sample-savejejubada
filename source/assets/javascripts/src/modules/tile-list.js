import { debounce } from 'throttle-debounce';
import Isotope from 'isotope-layout';
import Packery from 'isotope-packery'; // Import after `isotope-layout`

export default class {
  constructor(elem) {
    this.filterValue = '';
    this.$list = elem;
    this.$tiles = elem.children;
    this.iso = new Isotope(elem);
    this.isoDefaults = {
      getSortData: {
        address: '.address',
        name: '.name',
      },
      itemSelector: '.feature-tiles__item',
      layoutMode: 'packery',
      sortBy: 'name',
    }
  }

  addFilterControls() {
    const filterSel = this.$list.dataset.tileListFilter;
    this.$filter = document.querySelector(filterSel);

    if (!this.$filter) return;

    this.$filterInput = this.$filter.querySelector('.filter__input');
    this.$filterOutput = this.$filter.querySelector('.filter__output');

    this.$filterInput.addEventListener('input', debounce(200, (event) => {
      this.filterTiles(event.target.value);
    }));

    this.addFilterOutput();
  }

  addFilterOutput() {
    this.iso.on('arrangeComplete', function(filteredItems) {
      let resultText = '';

      if (this.filterValue) {
        if (filteredItems.length === 1) {
          resultText = '1 result';
        } else if (filteredItems.length) {
          resultText = `${ filteredItems.length } results`;
        } else {
          resultText = 'No results';
        }
      }

      this.$filterOutput.innerText = resultText;
    }.bind(this));
  }

  arrangeTiles() {
    this.iso.arrange(this.isoDefaults);
  }

  filterTiles(value) {
    value = value.toLowerCase();

    this.filterValue = value;

    this.iso.arrange({
      ...this.isoDefaults,
      filter: function($item) {
        const name = $item.querySelector('.name').innerText.toLowerCase();
        const address = $item.querySelector('.address').innerText.toLowerCase();

        return name.includes(value) || address.includes(value);
      },
    });
  }

  init() {
    this.addFilterControls();
    this.arrangeTiles();
  }
}
