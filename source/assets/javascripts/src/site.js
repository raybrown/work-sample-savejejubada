import { testFeatures } from './_utils';
import './modules/blur-up';
import MapModule from './modules/map';
import TileListModule from './modules/tile-list.js';

testFeatures(['js']);

for (const $map of document.querySelectorAll('[data-map]')) {
  new MapModule($map).init();
}

for (const $tileList of document.querySelectorAll('[data-tile-list]')) {
  new TileListModule($tileList).init();
}
