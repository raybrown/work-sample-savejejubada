const features = {
  js(node = document.documentElement) {
    node.classList.remove('no-js');
    node.classList.add('js');
  }
};

export function loadScript(url) {
  return new Promise(function(resolve) {
    const elem = document.createElement('script');
    elem.type = 'text/javascript';
    elem.src = url;

    document.head.appendChild(elem);

    elem.onload = function() {
      resolve();
    };
  });
}

export function loadStyles(url) {
  return new Promise(function(resolve) {
    const elem = document.createElement('link');
    elem.rel = 'stylesheet';
    elem.href = url;

    document.head.appendChild(elem);

    elem.onload = function() {
      resolve();
    };
  });
}

export function matchesDeep(element, selector) {
  return element.matches(selector) || element.closest(selector) ? true : false;
}

export function testFeatures(arr) {
  arr.forEach(item => { features[item](); });
}
