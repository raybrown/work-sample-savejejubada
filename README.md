> **Overview:**
>
> This project is a marketing website for Save Jeju Bada, a Korean nonprofit org that I befriended while living abroad in South Korea. I lived on an island off the southern coast called Jeju, and the nonprofit was created to educate people about coastal pollution and eco-friendliness. 
>
> When I met them, I offered to build them a website. I built the first version over a long weekend: https://savejejubada.com. It contains nothing other than a map that displays businesses that partner with the nonprofit. The version that I am sharing with you here is the _second version_ of the website (soon to be launched). It includes additional pages and information about the organization.
>
> Why I'm sharing this project:
>
> * It's solely my code
> * It utilizes Vue.js to build the map on the "Friends" page
> * It showcases my ability to work with multilingual content
> * It communicates that I know how to work with sensitive data (see `/.env-example` and the [dotenv docs](https://github.com/bkeepers/dotenv/blob/master/README.md))
> * It contains some open-source code that I've written (see `/helpers/image_helpers.rb`)
> * It conveys how I prefer to organize application styles (see `/source/assets/stylesheets/*`)
> * It shows my ability to write performant code (see [PageSpeed Insights results](https://developers.google.com/speed/pagespeed/insights/?url=https://savejejubada-v2.netlify.com/en/))

# Save Jeju Bada (v2)

__NOTE:__ This is the new version of the Save Jeju Bada website. It lives at [https://savejejubada-v2.netlify.com/](https://savejejubada-v2.netlify.com/).

This project is a small JAM-stack website built for Save Jeju Bada. Content is managed in [Contentful](https://www.contentful.com/) and then pulled into this [Middleman](https://middlemanapp.com/) static site, which is hosted on [Gitlab](https://gitlab.com/) and deployed via [Netlify](https://www.netlify.com/).

For more info, see the [Project Stack](https://gitlab.com/raybrown/work-sample-savejejubada/wikis/Project-Stack) wiki page.

## Requirements

* Ruby
* Bundler
* Node

## Setup

First, duplicate `.env.example` as `.env`. Then, open `.env` and supply the pertinent environment variables within that file. (If you're stuck on this step, reach out to [Ray](hey@raybrown.co).)

Then, run the following command, which installs all of the project's dependencies and pulls content down from Contentful:

```
bin/setup
```

## Development

Run the following command to start the local development server at [http://localhost:4567](http://localhost:4567):

```
middleman
```

Also, you should read about the project's [file/folder structure](https://gitlab.com/raybrown/work-sample-savejejubada/wikis/File-and-Folder-Structure) and checkout the [development guide](https://gitlab.com/raybrown/work-sample-savejejubada/wikis/Front-end-Development-Guide).

## Deployment

Automatic deploys to Netlify are triggered whenever:

* Content updates are made on Contentful
* New code changes are Git pushed to the remote `origin/master` branch

To deploy the site, simply push local Git changes like so:

```
git add . # ...or whatever changes you want to add
git commit -m "Your commit message"
git fetch && git status
git push # ...assuming there are no remote changes to pull down, of course
```

Once the code is pushed to the remote branch, Netlify will notice and begin a new deployment. As long as nothing fails, the new code will be deployed. If an error occurs, the previous version of the production website will remain intact and your new changes will not be deployed until the error is fixed.

## Manual Contentful Updates

To pull content from Contentful into Middleman manually, run the following command from the command line:

```
middleman contentful
```

This will connect to Contentful and generate a new YAML file for each Cafe and Beach Cleanup entry. This data lives in the `/data/contentful/` folder.

## Manual Builds

If you ever need to build the project for some reason (e.g. to troubleshoot a deployment issue), you can do so by running the following command:

```
bin/build
```

## Further Reading

Check out the following Wiki pages for more information about this project:

* [Project Stack](https://gitlab.com/raybrown/work-sample-savejejubada/wikis/Project-Stack)
* [File/Folder Structure](https://gitlab.com/raybrown/work-sample-savejejubada/wikis/File-and-Folder-Structure)
* [Front-End Development Guide](https://gitlab.com/raybrown/work-sample-savejejubada/wikis/Front-end-Development-Guide)
